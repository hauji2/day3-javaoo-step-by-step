package ooss;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class Teacher extends Person {

    private Set<Klass> klasses;

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
        this.klasses = new HashSet<>();
    }

    @Override
    public String introduce() {
        StringBuilder line = new StringBuilder();
        line.append("My name is ").append(this.getName()).append(". I am ").append(this.getAge()).append(" years old. I am a teacher.");

        if (!this.getKlasses().isEmpty()) {
            line.append(" I teach Class ");
            String classesTaught = this.klasses.stream().map(klass -> klass.getNumber().toString()).collect(Collectors.joining(", "));
            line.append(classesTaught);
            line.append(".");
        }

        return line.toString();
    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return this.klasses.contains(student.getKlass());
    }

    @Override
    public void update(Klass klass) {
        System.out.println("I am " + this.getName() + ", teacher of Class "
                + klass.getNumber() + ". I know "
                + klass.getClassLeader().getName() + " become Leader.");
    }
}
