package ooss;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class Student extends Person {

    private Klass klass;

    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        if (Objects.isNull(this.getKlass())) {
            return "My name is " + this.getName() + ". I am " + this.getAge() + " years old. I am a student.";
        }
        if (Objects.nonNull(this.getKlass().getClassLeader()) && this.getKlass().isLeader(this)) {
            return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.", this.getName(), this.getAge(), this.getKlass().getNumber());
        }

        return "My name is " + this.getName() + ". I am " + this.getAge() + " years old. I am a student. " + "I am in class " + this.getKlass().getNumber() + ".";
    }

    public void join(Klass klass) {
        this.setKlass(klass);
    }

    public boolean isIn(Klass klass) {
        if (Objects.isNull(this.getKlass()))
            return false;

        return Objects.equals(this.getKlass().getNumber(), klass.getNumber());
    }

    @Override
    public void update(Klass klass) {
        System.out.println("I am " + this.getName() + ", student of Class "
                + klass.getNumber() + ". I know "
                + klass.getClassLeader().getName() + " become Leader.");
    }
}
