package ooss;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder(toBuilder = true)
public class Klass {

    private Integer number;
    private Student classLeader;
    private List<Person> persons;

    public Klass(Integer number) {

        this.number = number;
        this.persons = new ArrayList<>();
    }

    public Klass(Integer number, Student classLeader) {

        this.number = number;
        this.classLeader = classLeader;
        this.persons = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Klass)) {
            return false;
        }
        Klass klass = (Klass) obj;
        return number.equals(klass.getNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student student) {
        if (Objects.isNull(student.getKlass()) || !Objects.equals(this.getNumber(), student.getKlass().getNumber())) {
            System.out.println("It is not one of us.");
            return;
        }
        this.classLeader = student;
        System.out.println(this.classLeader.introduce());

        notifyAllObservers();
    }

    public boolean isLeader(Student student) {
        return this.classLeader.equals(student);
    }

    public void attach(Person observer) {
        this.persons.add(observer);
    }

    public void notifyAllObservers() {
        persons.stream().forEach(person -> person.update(this));
    }
}
